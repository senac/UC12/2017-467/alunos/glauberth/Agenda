<%@page import="br.com.senac.agenda.model.Usuario"%>
<%@page import="java.util.List"%>
<jsp:include page="../header.jsp"/>


<% List<Usuario> lista = (List) request.getAttribute("lista");%>
<% String mensagem = (String) request.getAttribute("mensagem");%>


<% if (mensagem != null) {%>

<div class="alert alert-danger">
    <%=mensagem%>
</div>

<%}%>

<fieldset>

    <center> <b><legend style="color: darkcyan">Pesquisa de Usu�rios</legend> </center></b>

<form class="form-inline" action="./PesquisaUsuarioServlet">

    <div class="form-group" style="padding: 20px;">
            <label for="txtCodigo" style="margin-right: 10px">C�digo:</label> <input name="id" class="form-control form-control-sm" id="txtCodigo" type="text"/>
    </div>

    <div class="form-group">
        <label for="nome" style="margin-right: 10px" >Nome:</label> <input name="nome" id="nome"  class="form-control form-control-sm "type="text"/>
    </div>
    <div> 
        <button style="margin-left: 10px"type="submit" class="btn btn-default"><i class="fas fa-search"></i>Pesquisar</button>
    </div>
</form>
</fieldset>

<hr />

<table class="table table-hover">
    <thead>
        <tr>
            <th>C�digo</th> <th>Nome</th>
        </tr>
    </thead>

    <% if (lista != null && lista.size() > 0) {
            for (Usuario u : lista) {
    %>
    <tr>
        <td><%= u.getId()%></td><td><%= u.getNome()%></td> <td> <a href='./SalvaUsuarioServlet?id=<%u.getId();%> ' > 
                <img src="../resources/imagens/Users-Add-User-icon.png"/></a>
        </td>

        <td> <a href="" data-toggle="modal" data-target="#Exclusao" 
                onclick="excluir(<%= u.getId()%> , '<%= u.getNome()%>');">
                <img src="../resources/imagens/Button-Delete-icon.png"/></a>
        </td>

    </tr>



    <% }

    } else {%>

    <tr >
        <td  colspan="2">No Existem registros.</td>
    </tr>

    <%}%>


    <div class="modal fade" id="Exclusao" tabindex="-1" role="dialog" 
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excluir</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Deseja realmente apagar este usu�rio <span id="nomeUsuario"></span> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                    <a  id="btnConfirmar" class="btn btn-primary">Confirmar</a>
                </div>
            </div>
        </div>
    </div>




</table>



<jsp:include page="../footer.jsp"/>