
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.UsuarioDAO;
import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala302b
 */
@WebServlet(name = "SalvaUsuarioServlet", urlPatterns = {"/usuario/SalvaUsuarioServlet"})
public class SalvaUsuarioServlet extends HttpServlet {

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Usuario usuario = null;
        String erro = null;
        String id = request.getParameter("id");

        try {
            int codigo;

            try {

                codigo = Integer.parseInt(id);

            } catch (NumberFormatException e) {
                codigo = 0;

            }

            UsuarioDAO dao = new UsuarioDAO();
            usuario = dao.get(codigo);
            request.setAttribute("usuario", usuario);

        } catch (Exception e) {
            erro = "Usuario não encontrado";
            request.setAttribute("erro", erro);
            e.printStackTrace();

        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("gerenciarUsuario.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String erro = null;

        String id = request.getParameter("id");
        String nome = request.getParameter("nome");
        String senha = request.getParameter("senha");
        
        
        try {
            
            Usuario usuario = new Usuario();
            
            int codigo;   
            
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }
            
            usuario.setId(codigo);
            usuario.setNome(nome);
            usuario.setSenha(senha);
                

            UsuarioDAO dao = new UsuarioDAO();
            dao.salvar(usuario);

            String salvo = "Salvo com sucesso!";

            request.setAttribute("usuario", usuario);
            request.setAttribute("salvar", salvo);

        } catch (Exception e) {

            erro = "Erro ao cadastrar";
            request.setAttribute("erro", erro);

        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("gerenciarUsuario.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
    
    /*
    
        String erro = null;

        String codigo = request.getParameter("id");
        String nome = request.getParameter("nome");
        String senha = request.getParameter("senha");
        Integer id = null;

        try {
            Usuario usuario = new Usuario();

            usuario.setId(0);
            usuario.setNome(nome);
            usuario.setSenha(senha);

            if (codigo != null && !codigo.trim().isEmpty()) {
                id = new Integer(codigo);
            }

            UsuarioDAO dao = new UsuarioDAO();
            dao.salvar(usuario);

            String salvo = "Salvo com sucesso!";

            request.setAttribute("usuario", usuario);
            request.setAttribute("salvar", salvo);

        } catch (Exception e) {

            erro = "Erro ao cadastrar";
            request.setAttribute("erro", erro);

        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("gerenciarUsuario.jsp");
        dispatcher.forward(request, response);

    */

}
