<%@page import="br.com.senac.agenda.model.Usuario"%>
<jsp:include page="../header.jsp"/>

<hr/>

<% String mensagem = (String) request.getAttribute("salvar");%>
<% Usuario usuario = (Usuario) request.getAttribute("usuario");  %>
<% String erro = (String) request.getAttribute("erro");%>

    <% if(mensagem != null) {%>

    <div class="alert alert-success">    
        <%= mensagem%>
    </div>
    <%}%>
    
    
   <% if(erro != null) {%>

    <div class="alert alert-danger">    
        <%= erro%>
    </div>
    <%}%>
    
<form  action="./SalvaUsuarioServlet" method="post">
    
    <div class="form-group">
    <label for="codigo">C�digo:</label>
    <p><input name="codigo" type="text" id="codigo" value="<%= usuario != null ? usuario.getId() : " " %>"  class="form-control col-2" readonly=""/></p>
  </div>
    
  <div class="form-group">
    <label for="codigo">Nome:</label>
    <input name="nome" type="text" class="form-control" id="nome"  value="<%= usuario != null ? usuario.getNome() : " "%>" placeholder="Digite um nome.."/>
  </div>
    
   <div class="form-group">
    <label for="senha">Senha:</label>
    <input name="senha" type="password" class="form-control" id="senha" placeholder="Digite sua senha.."/>
  </div>
    
    <center><button type="submit" style="font-family: monospace" class="btn btn-primary mb-2">Salvar</button>
    <button type="reset" style="font-family: monospace "  class="btn btn-outline-danger mb-2" >Cancelar</button></center>
   
</form>

<jsp:include page="../footer.jsp"/>