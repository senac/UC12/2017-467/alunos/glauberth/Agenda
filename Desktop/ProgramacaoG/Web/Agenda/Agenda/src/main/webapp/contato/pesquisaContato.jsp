<%@page import="java.util.List"%>
<%@page import="br.com.senac.agenda.model.Contato"%>
<jsp:include page="../header.jsp"/>

<% List<Contato> lista = (List) request.getAttribute("lista"); %>
<% String mensagem = (String) request.getAttribute("mensagem"); %>

<% if (mensagem != null) {%>

<div class="alert alert-danger">
    <%=mensagem%>
</div>

<%}%>

<fieldset>

    <center> <b><legend style="color: darkblue">Pesquisa de Contatos</legend> </center></b>

<form action="./PesquisaContatosServlet" method="post">
    <div class="form-row">

        <div class="form-group col-md-2">
            <label for="id">Id</label>
            <input name="id" type="text" class="form-control form-control-md" id="id">
        </div>    

        <div class="form-group col-md-4">
            <label for="nome">Nome</label>
            <input name="nome" type="text" class="form-control form-control-md" id="nome">
        </div>  

        <div class="form-group col-md-2">
            <label for="estado">Estado</label>
            <select name="estado" class="form-control form-control-md" id="estado">                
                <option selected="true"></option>
                <option>ES</option>
                <option>SP</option>
                <option>RJ</option>
                <option>MG</option>
                <option>BH</option>
            </select>        
        </div>
        <div class="form-group col-md-4" style="padding-top: 30px">
            <button type="submit" class="btn btn-outline-primary">Pesquisar</button>        
        </div>
    </form>
</fieldset>

<hr/>


<table class="table table-hover">

    <thead>
        <tr>
            <th>C�digo</th> <th>Nome</th> <th>Telefone</th>
            <th>Celular</th><th>Email</th> <th></th>
        </tr>
    </thead>    

    <% if (lista != null && lista.size() > 0) {
            for (Contato c : lista) {
    %>


    <tr>
        <td><%= c.getId()%></td> <td><%= c.getNome()%></td> <td> <%= c.getTelefone()%> </td> <td><%= c.getCelular()%></td> <td><%= c.getEmail()%></td> 

        <td> <a href='./CadastraUsuarioServlet?id=<%= c.getId()%>' > 
                <img src="../resources/imagens/Users-Add-User-icon.png"/></a>
        </td>


        <td>    <a href="" data-toggle="modal" data-target="#Exclusao" 
                   onclick="excluir(<%= c.getId()%>, '<%= c.getNome()%>');">
                <img src="../resources/imagens/Button-Delete-icon.png"/></a>
        </td>


    </tr>


    <% }
    } else {%>

    <tr >
        <td  colspan="2">N�o existem registros na pesquisa!!!</td>
    </tr>

    <%}%>


    <div class="modal fade" id="Exclusao" tabindex="-1" role="dialog" 
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excluir</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Deseja realmente apagar este contato <span id="nomeUsuario"></span>?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">N�O</button>

                    <a  id="btnConfirmar" class="btn btn-primary">SIM</a>
                </div>
            </div>
        </div>
    </div>


</table>


<center><button type="reset" class="btn btn-danger" style="font-family: monospace">Cancelar</button</center>   

<jsp:include page="../footer.jsp"/>
