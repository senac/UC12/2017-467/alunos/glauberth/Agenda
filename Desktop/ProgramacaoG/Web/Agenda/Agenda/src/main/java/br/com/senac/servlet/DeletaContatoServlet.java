/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.ContatoDAO;
import br.com.senac.agenda.dao.UsuarioDAO;
import br.com.senac.agenda.model.Contato;
import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala302b
 */
@WebServlet(name = "DeletaContatoServlet", urlPatterns = {"/contato/DeletaContatoServlet"})
public class DeletaContatoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String erro = null;
        String nome = request.getParameter("nome");
        String estado = request.getParameter("estado");

        try {
            Integer codigo = null;

            if (id != null && !id.trim().isEmpty()) {
                codigo = Integer.parseInt(id);
            }

            ContatoDAO dao = new ContatoDAO();
            Contato contato = new Contato();
            contato.setId(codigo);

            dao.deletar(contato);

            String mensagem = "Removido com sucesso";

            List<Contato> lista = dao.getByPesquisa(codigo, nome,estado);

            request.setAttribute("lista", lista);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception e) {
            
            erro = "Contato não encontrado.";
            request.setAttribute("erro", erro);
            e.printStackTrace();

        }
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("pesquisaContato.jsp");
        dispatcher.forward(request, response);
                
    }

}
