/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.ContatoDAO;
import br.com.senac.agenda.model.Contato;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CadastraUsuarioServlet", urlPatterns = {"/contato/CadastraUsuarioServlet"})
public class CadastraUsuarioServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("id");
        String erro = null;

        Integer id = null;

        if (codigo != null && !codigo.trim().isEmpty()) {
            id = Integer.parseInt(codigo);

        } else {
            erro = "Não foi possivel encontrar contato";
        }

        ContatoDAO dao = new ContatoDAO();
        Contato contato = dao.get(id);

        request.setAttribute("contato", contato);
        request.setAttribute("erro", erro);

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastroUsuario.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("id");
        String nome = (String) request.getParameter("nome");
        String telefone = (String) request.getParameter("telefone");
        String celular = (String) request.getParameter("celular");
        String cep = (String) request.getParameter("cep");
        String endereco = (String) request.getParameter("endereco");
        String numero = (String) request.getParameter("numero");
        String bairro = (String) request.getParameter("bairro");
        String cidade = (String) request.getParameter("cidade");
        String email = (String) request.getParameter("email");
        String estado = (String) request.getParameter("estado");

        Integer id = null;

        try {

            Contato contato = new Contato();
            contato.setNome(nome);
            contato.setTelefone(telefone);
            contato.setCelular(celular);
            contato.setEndereco(endereco);
            contato.setCep(cep);
            contato.setNumero(numero);
            contato.setBairro(bairro);
            contato.setCidade(cidade);
            contato.setEmail(email);
            contato.setEstado(estado);

            if (codigo != null && !codigo.trim().isEmpty()) {
                id = Integer.parseInt(codigo);

            } else {
                id = 0;
            }

            ContatoDAO dao = new ContatoDAO();
            dao.salvar(contato);

            String mensagem = "Salvo com Sucesso!";

            request.setAttribute("contato", contato);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception e) {
            String erro = "Erro de Cadastro";
            request.setAttribute("erro", erro);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastroUsuario.jsp");
        dispatcher.forward(request, response);

    }

}
