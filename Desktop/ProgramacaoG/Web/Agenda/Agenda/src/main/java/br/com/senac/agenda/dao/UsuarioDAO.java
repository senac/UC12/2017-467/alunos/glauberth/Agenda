package br.com.senac.agenda.dao;

import br.com.senac.agenda.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO extends DAO<Usuario> {

    @Override
    public void salvar(Usuario usuario) {

        Connection connection = null;

        try {

            String query;
            if (usuario.getId() == 0) {
                query = "INSERT INTO usuario(nome , senha) values (? , ? );";

            } else {
                query = "update usuario set nome = ? , senha = ? where id = 4 ;";
            }
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, usuario.getNome());
            ps.setString(2, usuario.getSenha());

            if (usuario.getId() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                usuario.setId(rs.getInt(1));
            } else {
                ps.setInt(3, usuario.getId());
                ps.executeUpdate();
            }

        } catch (Exception e) {

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro de conexao");
            }
        }

    }

    @Override
    public void deletar(Usuario usuario) {

        String query = "delete from usuario where id = ?;";
        Connection connection = null;

        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, usuario.getId());
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("ERRO AO DELETAR REGISTRO");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao fechar conexao");
            }

        }

    }

    @Override
    public List<Usuario> listar() {

        String query = "SELECT * FROM usuario;";

        List<Usuario> lista = new ArrayList<>();

        Connection connection = null;

        try {

            connection = Conexao.getConnection(); //Abrir conexao com banco
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);  // Executa a query e retorna uma tabela

            while (rs.next()) {
                Usuario usuario = new Usuario();

                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setSenha(rs.getString("senha"));
                lista.add(usuario);
            }

        } catch (Exception e) {
            System.out.println("Erro ao consultar lista");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao fechar conexao");
            }
        }

        return lista;
    }

    @Override
    public Usuario get(int id) {

        Usuario usuario = null;
        Connection connection = null;
        String query = "SELECT * FROM usuario WHERE id = ? ; ";

        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                usuario = new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setSenha(rs.getString("senha"));
            }

        } catch (Exception e) {
            System.out.println("Erro ao executar consulta");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao fechar conexao");
            }
        }
        return usuario;
    }

    public Usuario getByName(String name) {

        Usuario usuario = null;
        Connection connection = null;
        String query = "SELECT * FROM usuario WHERE nome = ? ; ";

        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                usuario = new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setSenha(rs.getString("senha"));
            }

        } catch (Exception e) {
            System.out.println("Erro ao executar consulta!!!");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao fechar conexao");
            }
        }
        return usuario;

    }

    public List<Usuario> getByFiltro(Integer id, String name) {

        List<Usuario> lista = new ArrayList<>();
        Connection connection = null;

        try {
            StringBuilder sb = new StringBuilder("select * from usuario where 1 = 1 ");

            if (id != null) {

                sb.append(" and id = ? ");
            }
            if (name != null && !name.trim().isEmpty()) {
                sb.append(" and nome like = ? ");
            }
            
            
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(sb.toString());

            int index = 0;

            if (id != null) {
                ps.setInt(++index, id);
            }

            if (name != null && !name.trim().isEmpty()) {
                ps.setString(++index, "$" + name + "$");
            }

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setSenha(rs.getString("senha"));
                lista.add(usuario);
            }
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar!");
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println("Erro ao fechar conexão com BANCO DE DADOS");
            }

            return lista;
        }

    }
    
    
}
